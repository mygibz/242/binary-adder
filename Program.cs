﻿using System;

namespace binaryThingie {
    class Program {
        static void Main(string[] args) {
            // Input data
            string input1 = "101";
            string input2 = "1101";

            int inputLength = (input1.Length > input2.Length ? input1.Length : input2.Length);

            // Make strings the same length
            if (input1.Length < inputLength)
                input1 = new String('0', inputLength - input1.Length) + input1;
            else if (input2.Length < inputLength)
                input2 = new String('0', inputLength - input2.Length) + input2;

            // Do calculations
            int i = inputLength - 1;
            bool rest = false;
            string output = "";
            do {
                // Do main calculation
                int result = (input1[i] == '1' ? 1 : 0) + (input2[i] == '1' ? 1 : 0) + (rest ? 1 : 0);
                Console.WriteLine($"{(input1[i] == '1' ? 1 : 0)} + {(input2[i] == '1' ? 1 : 0)} + {(rest ? 1 : 0)} = {result}");

                // Give correct output, based on that calculation
                rest = (result == 2 || result == 3);
                output = result % 2 + output;

                i--;
            } while (i >= 0);
            
            // Output result (also account for rest, if present)
            Console.WriteLine((rest ? "1" : "") + output);
        }
    }
}
